========================
Running Raku notebook
========================

As I am more familiar with Docker, I am explaining the docker way of
running Raku notebook. Of course there are other ways to achieve the
same.

Step 1: Docker installation
^^^^^^^^^^^^^^^^^^^^^^^^^^^

To run Raku notebook, first install ``Docker`` in your respective
operating system.

As I am running Windows, I install Docker `from here`_.

Go to your docker terminal and find ``ip-address`` with the command
``docker-machine ip``:

|image0|

Step 2: Pulling the Docker image with Raku kernel
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

From the docker terminal, pull the image with command:
``docker pull sumankhanal/raku-notebook``.

|image1|

Step 3: Running the container
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the docker terminal, type the command:
``docker run -it -p 8888:8888 sumankhanal/raku-notebook``.

|image2|

Note the token number.

Step 4: Accessing the running container
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Open your web browser and type:

``ip-address:8888``. In my case, I type: ``http://192.168.99.100:8888/``
as my ``ip-address`` is: ``192.168.99.100``. You will be greeted with
such window:

|image3|

Step 5: Login with token number
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After inserting the token number(which is shown above), you will get
such webpage:

|image4|

On the right upper corner, you will find ``New`` button. Click on that
and you will get Raku as option. Click on Raku.

|image5|

After that you will have Raku notebook running.

|image6|

Step 6: Mounting the host folder into docker folder
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You may be having Raku files (``.raku``) or Raku notebooks (that is
``.ipynb`` files) in a folder in your PC that you want to work inside
docker. In that case you have to **mount** that folder inside docker.

Here I will give one example. I have my Raku files in a folder (Note I
am in ``Windows 10``) named ``C:\Users\Suman\Desktop\Raku`` in my host
which I mount on ``/tmp`` folder and make it as working directory inside
docker with the command
``docker run -it -p 8888:8888 -v /c/Users/Suman/Desktop/Raku:/tmp -w /tmp sumankhanal/raku-notebook``
which is clearly shown in figure below: |image7|

Step 7: Accessing the Raku files of the host
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After following above instructions and inserting the token number you
will be in your mounted folder with contents of the host folder. In my
case I mounted on ``/tmp``.

|image8|

Then if you want to open your Raku notebook, click the respective
``.ipynb`` file.

.. _from here: https://github.com/docker/toolbox/releases

.. |image0| image:: https://i.imgur.com/Q8Zya9N.png
.. |image1| image:: https://i.imgur.com/7kIugoV.png
.. |image2| image:: https://i.imgur.com/nzzfYLB.png
.. |image3| image:: https://i.imgur.com/1ImEkFt.png
.. |image4| image:: https://i.imgur.com/QGC7UrS.png
.. |image5| image:: https://i.imgur.com/SmEcrR4.png
.. |image6| image:: https://i.imgur.com/9zSYnJI.png
.. |image7| image:: https://i.imgur.com/PdOLFAQ.png
.. |image8| image:: https://i.imgur.com/R8ytgqS.png
