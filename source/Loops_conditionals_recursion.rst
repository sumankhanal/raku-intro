==================================
Loops, conditionals, and recursion
==================================

Conditional
~~~~~~~~~~~

Ternary operator
^^^^^^^^^^^^^^^^

**Syntax**

::

      [Boolean-valued condition] ?? [expression to evaluate if true] !! [expression to evaluate if false]

**Example**

.. code-block:: perl6
    :linenos:



      my $score = 70;
      my $result = ( $score > 60 ) ?? 'Pass' !! 'Fail';
      say $result

      # Output

      Pass
