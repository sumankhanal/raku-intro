.. Introduction to Raku documentation master file, created by
   sphinx-quickstart on Mon Sep 25 18:37:31 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Introduction to Raku
==================================================

.. warning::

  **THIS DOCUMENT IS UNDER CONSTRUCTION! WORK IN PROGRESS!**

Contents:

.. toctree::
   :maxdepth: 2
   
   Introduction.rst
   Basics.rst
   raku_notebook.rst
   Variables_and_scope.rst
   Functions.rst
   Loops_conditionals_recursion.rst
   Iteration.rst
   Strings.rst
   Arrays_and_lists.rst
   Hashes.rst
   Classes_and_objects.rst
   Regexes_and_grammars.rst
   Functional_programming.rst
   modules.rst
   Examples.rst
   exercises.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
