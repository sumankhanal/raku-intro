=======================
Creating Raku modules
=======================


A module is usually a source file or set of source files that expose Raku constructs. These are typically packages (classes, roles, grammars), subroutines, and sometimes variables.

Module distributions (in the set of related source files sense) in Raku have a main project directory containing a ``README`` and a ``LICENSE`` file, a ``lib`` directory for the source files, which may be individually referred to as modules and/or may themselves define modules with the module keyword , a ``t`` directory for tests, and possibly a ``bin`` directory for executable programs and scripts.

Source files generally use the ``.rakumod`` extension, and scripts or executables use the ``.raku``. Test files use the ``.t`` or ``.rakutest`` extension. Files which contain documentation use the ``.rakudoc`` extension.


*********
Plain way
*********

# TODO



**************
Using App::Mi6
**************

Creating modules has become easier with `App:Mi6 <https://github.com/skaji/mi6>`_ package.

To use this tool, first get it installed with command:

``zef install App::Mi6``

Or you can use `this docker image directly <https://hub.docker.com/r/sumankhanal/rakudo>`_. 

``App::Mi6`` is a minimal authoring tool for Raku. Features are:

  * Create minimal distribution skeleton for Raku

  * Generate ``README.md`` from ``lib/Main/Module.rakumod``

  * Run tests by ``mi6 test``

  * Release your distribution tarball to CPAN




.. list-table:: Few **App::Mi6** functions
   :widths: 25 55
   :header-rows: 1

   * - Function
     - Use
     
   * - **mi6 new Foo::Bar**
     - Create Foo-Bar distribution
   * - **mi6 build**
     - Build the distribution and re-generate README.md/META6.json
   * - **mi6 test**
     - Run tests
   * - **mi6 release**
     - Release your distribution to CPAN
     
*******     
Example
*******

When you run ``mi6 new Foo::Bar`` in terminal, it will create directory ``Foo-Bar`` inside which following files and directories will be found:

.. image:: _static/module.PNG
  :width: 600

 
+ ``lib`` directory contains source files
+ ``bin`` directory for executable programs and scripts
+ ``t`` directory for tests. Test files use the ``.t`` or ``.rakutest`` extension
+ ``META6.json`` containing metadata
+ ``README.md`` file which is a markdown-formatted text file, which will later be automatically rendered as HTML by GitHub
+ ``LICENSE`` file
+ ``.travis.yml`` file for continuous integration
+ ``dist.ini`` file to customise ``mi6`` behavior
