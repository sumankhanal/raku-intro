=======
Strings
=======


The Raku language handles all strings as UTF-8 by default. Strings can be enclosed within single quote or double quotes or various quote-like operators. Double quotes allow for string interpolation.

A string can be declared as:

.. code-block:: perl6
    :linenos:

    my Str $string = 'Limpiyadhura is in Nepal';

Strings can be appended to one another, using the concatenation operator: ``~``.

.. code-block:: perl6
    :linenos:

    my Str $str = "Hello" ~ " World" ~ "!";
    say $str; # Hello World!
    $str.say; # same as above