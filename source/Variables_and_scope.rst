===================
Variables and scope
===================


A variable is a container that has a name and holds a value.
An **assignment statement** uses the equals sign ``=`` and gives a value to a variable, but, before
you can assign a value to a variable, you first need to create the variable by declaring it (if
it does not already exist):

.. code-block:: perl6
    :linenos:

    > my $message; # variable declaration, no value yet
    > $message = 'And now for something completely different';
    And now for something completely different
    > my $number = 42; # variable declaration and assignment
    42
    > $number = 17; # new assignment
    17
    
    # Binding operator, you cannot reassign the value to a variable with binding operator
    
    > my $second_number := 40;
    40
    > $second_number = 60;
    Cannot assign to an immutable value
      in block <unit> at <unknown file> line 1

In Raku, variable names start with a so-called sigil such as ``$``, ``@``, ``%``, ``&``, and some others.

The keyword ``my`` is a way of declaring a new variable. Whenever you create a new variable in Raku,
you need to declare it, i.e., tell Raku that you’re going to use that new variable; this is most commonly
done with the ``my`` keyword, which declares a lexical variable.


Raku variable names
*******************

Variable names can be as long as you like. 

+ Can contain both letters and numbers
+ User-defined variable names **can’t begin with a number**
+ Variable names are **case-sensitive**, i.e., ``$message`` is not the same variable as ``$Message`` or ``$MESSAGE``.
+ Can contain **Unicode** letters

It is legal to use uppercase letters, but it is conventional to use only lower case for most variables names. Some people
nonetheless like to use ``$TitleCase`` for their variables or even pure ``$UPPERCASE`` for some
special variables.

Unlike most other programming languages, Raku does not require the letters and digits
used in variable names to be plain ASCII. You can use all kinds of Unicode letters
which can be useful for non-English programmers (provided that these Unicode characters are
handled correctly by your text editor and your screen configuration).


Raku as a Calculator
********************


Raku can be used as a calculator. It supports addition, subtraction, multiplication, division, exponentiation, grouping. It has rich inbuilt mathematical functions. More exciting is the support for fractions.

.. code-block:: perl6
    :linenos:
    
    # addition
    3 + 5 # 8

    # division
    3/5 # 0.6

    # modulo 
    4 % 2 # 0
    4 mod 2 # 0

    # exponentiation
    2 ** 3 # 8

    # Adding fractions 4/5 and 2/3
    (4/5 + 2/3).raku # <22/15>

    # Adding fractions 4/5 and 4/5
    (4/5 + 4/5).nude.join("/") # 8/5

    # logarithmic functions
    # log of 10 to the base e (default)
    log(10) # 2.302585092994046

    # log of 10 to the base 2
    log(10,2) # 3.3219280948873626

    # trigonometric functions
    sin(3) #  0.1411200080598672

    # pi is inbuilt into raku
    pi # 3.141592653589793
    pi/2 # 1.5707963267948966

    # parenthesis
    # Note the order of operations
    # 1. Parentheses 2. Exponentiation 3. Multiplication and Division 4. Addition and Subtraction
    my $x = pi/2;
    say 2*$x*(1.5 - $x)**4; # 7.892103531358613e-05


Using the **order of operations**, let us find the roots of a quadratic equation :math:`x^2 - 4x + 3 = 0` with the help of following formula:

.. math::
   
   x = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a}


The roots are:

.. code-block:: perl6
    :linenos:

    # for the given equation respective values for a, b and c are:
    my $a = 1;
    my $b = -4;
    my $c = 3;
    # first root $x  
    my $x = (-$b + sqrt($b**2 - 4*$a*$c))/(2*$a);
    $x # 3

    # second root $y
    my $y = (-$b - sqrt($b**2 - 4*$a*$c))/(2*$a);
    $y # 1




Raku variables
**************

Raku variables are classified into 3 categories: Scalars, Arrays and Hashes.