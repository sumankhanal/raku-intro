=====================
Practical Examples
=====================


Following codes may be unoptimised version, but shows how it is implemented for beginners.

**This example** shows the number of elements that contain digit "7" only twice in a range starting
from ``0`` to ``num`` in Raku.


.. code-block:: perl6
    :linenos:
    
    sub count($num) {
        my @lst;
        for 0..$num -> $v {
            if $v.Str.indices("7").elems == 2 {
                @lst.push($v)
              }
         }
         return @lst.elems
         }

    print count(99999)



Same case implemented in Python 3 in exactly the same way. Here's the code.
 


.. code-block:: python
    :linenos:

    def count(num):
        lst=[]
        for i in range(num):
            if str(i).count("7") == 2:
                lst.append(i)
        return len(lst)

    print(count(99999))