==================================
Raku Basics: Running the program
==================================

Raku programs can be run in

+ The terminal **or**
+ The docker container **or** 
+ The `Atom editor <https://atom.io/>`_ with `script plugin <https://github.com/perl6/atom-language-perl6>`_ installed **or**
+ The `Visual Studio Code <https://code.visualstudio.com/>`_ with `Code Runner <https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner>`_ installed
+ **Comma IDE**


Running in the terminal
***********************

Make sure ``raku`` binary is in your path. Lets see an example how we can run Raku programs:

.. code-block:: raku
    :linenos:

    say "Hello World!"

Save the above program in ``hello.raku`` file and run in the terminal by typing:
``raku hello.raku``. You will see the output ``Hello World!``.
Congratulations for running your first Raku program!

Other raku scripts can be run similarly.


Running in Docker container
***************************

It is the most versatile way of running ``raku`` programs independent of OS. After installing Docker in your OS, go to the interactive shell, 
then pull and run the ``rakudo`` image from DockerHub. Here is one example:

``docker run -it -v $PWD:/tmp -w /tmp sumankhanal/rakudo:2020.05.1 bash``

This command pulls (if not present locally) and runs the ``sumankhanal/rakudo:2020.05.1`` image interactively, 
at the same time mounts the ``$PWD`` inside ``/tmp`` folder of the container and makes ``/tmp`` as the current working directory, 
where you will see the same files as contained in ``$PWD``. Run your scripts with command ``raku <program>.raku``

For more information and running notebooks, see `here <https://raku-intro.readthedocs.io/en/latest/raku_notebook.html>`_.


----------

Running in Atom IDE
*******************

For running ``.raku`` programs in Atom IDE, make sure `script plugin <https://github.com/rgbkrk/atom-script>`_ and `raku plugin <https://github.com/perl6/atom-language-perl6>`_ are 
installed, and ``raku`` binary is in your path.
After that, simply use the ``Ctrl+Shift+B`` keyboard shortcut that runs your program.
``Atom`` is easier to use and can be highly customised.

Running in VS Code
******************

For running ``.raku`` programs in VS Code, make sure the `Code Runner plugin <https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner>`_
is installed and ``raku`` binary is in your path.
After that, use keyboard shortcut ``Ctrl+Alt+N`` to run the program.


Running in the Comma IDE
************************

Another option is to use full-fledged Raku IDE aka `Comma IDE <https://commaide.com/>`_. It provides detailed syntax highlighting, auto-completion, 
debugger, grammar support, code inspection, integrated test runner with profiler.


