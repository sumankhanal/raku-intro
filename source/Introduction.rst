============
Introduction
============


Raku is a member of the Perl family of programming languages.
It has introduced elements of many modern and historical languages.
Compatibility with Perl is not a goal, Raku is different from Perl. However,
Perl modules can be used in Raku with the help of `Inline::Perl <https://github.com/niner/Inline-Perl5>`_.

The Raku language has version numbers ``v6.c``, ``v6.d``, ``v6.e``, and so forth.
These are very much like ``C`` has ``C89``, ``C99``, and ``C11``. Each language version is defined by its test suite.
Rakudo is a Raku implementation, as ``GCC``, ``Clang``, and ``MSVC`` are implementations of the ``C`` language.
While `historically several implementations <https://www.raku.org/compilers/>`_
were being written, today only the Rakudo implementation is in active development. For more details,
see `here <https://github.com/rakudo/rakudo/blob/master/docs/language_versions.md>`_.


See `Raku website here <https://raku.org/>`_. For Wikipedia reference and
overview `see here <https://en.wikipedia.org/wiki/Raku>`_.

Raku is a high-level, clean, modern, multi-paradigm language that
offers procedural, object-oriented AND functional programming methodologies, parallelism, 
concurrency, asynchrony, definable grammars for pattern matching and generalized string processing, 
and optional and gradual typing.

This book is written to give an introduction to programming using Raku. Please note that this is a **work in progress**.


Contributors
************

+ Dr Suman Khanal
+ Ashish Khanal
